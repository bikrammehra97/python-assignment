#Read 10 numbers from user and find the average of all. Q12
list1 = []

avg = 0.0
less_than_avg= 0
greater_than_avg = 0
equal = 0
for i in range(0,10):
    list1.append(float(input()))
    avg+=(list1[i]/10)

for ele in list1:
    if(ele<avg):
        less_than_avg+=1
    elif(ele>avg):
        greater_than_avg+=1
    else:
        equal+=1
print("Less than average are  "+str(less_than_avg))
print("Greater than average are  "+str(greater_than_avg))
print("Equal to the average are  "+str(equal))