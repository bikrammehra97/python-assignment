#Command Line arguments Q5 (b) Print largest of three numbers
import sys

arguments = sys.argv

if(arguments[1]>arguments[2] and arguments[2]>arguments[3]):
    print("Biggest is \t:",arguments[1])
elif(arguments[2]>arguments[1] and arguments[2]>arguments[3]):
    print("Biggest is \t:",arguments[2])
else:
    print("Biggest is \t:",arguments[3])