"""
Write program to perform following:
     i) Check whether given number is prime or not.
    ii) Generate all the prime numbers between 1 to N where N is given number.
"""

#i) Check whether given number is prime or not.
number = int(input("Enter any number to check if it is prime or not:\t"))

if(number>1):
    for i in range(2,number):
        if(number%i == 0):
            print(number," is not Prime")
            break
        else:
            print(number," is Prime")
            break
else:
    print(number," is not Prime")
#ii) Generate all the prime numbers between 1 to N where N is given number.
print("Enter a range starting from 1 upto we need to print Prime numbers:\n")
limit =int(input())

for num in range(1,limit+1):
    if(num>2):
        for i in range(2,num):
            if(num%i == 0):
                print(num," is not Prime")
                break
            else:
                print(num," is Prime")
                break  
    elif(num==2):
        print(num," is Prime")
    else:
        print(num," is not Prime")
    