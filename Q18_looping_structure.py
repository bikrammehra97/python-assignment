"""
Using loop structures print numbers from 1 to 100.  and using the same loop print numbers from 100 to 1 (reverse printing)
a) By using For loop 
b) By using while loop
c) Let mystring ="Hello world"
print each character of mystring in to separate line using appropriate loop structure.
"""
#a) By using For loop
for i in range(1,101):
    print(i)
for i in range(101,0,-1):
    print(i)
#a) By using while loop
n1=1,n2=101
while(n1!=101 and n2!=0):
    print(n1,"\t",n2)
    n1+=1
    n2-=1
#c) Let mystring ="Hello world"
#print each character of mystring in to separate line using appropriate loop structure.
inputstring = input("Enter any string to print in separate line:\n")
