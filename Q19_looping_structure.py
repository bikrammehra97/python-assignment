"""
Using loop structures print even numbers between 1 to 100.  
a) By using For loop, use continue/ break/ pass statement to skip odd numbers.
    i) Break the loop if the value is 50
    ii) Use continue for the values 10,20,30,40,50
b) By using while loop, use continue/ break/ pass statement to skip odd numbers.
      i) Break the loop if the value is 90
      ii) Use continue for the values 60,70,80,90

"""
for ele in range(1,101):
    if(ele==50):
        break
    if ele in [10,20,30,40,50]:
        continue
    elif(ele%2 == 0):
        print(ele, "\t Even")
    else:
        print(ele, "\t Odd")
n = 1

while(n!=101):
    if n in [60,70,80,90]:
        n+=1
        continue
    elif n%2 == 0:
        print(n,"\t Even")
    else:
        print(n,"\t Odd")
    n+=1