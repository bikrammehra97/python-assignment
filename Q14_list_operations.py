"""Write a program to create two list A & B such that List A contains Employee Id, List B contain Employee name (minimum 10 entries in each list) & perform following operation
     a) Print all names on to screen
     b) Read the index from the  user and print the corresponding name from both list.
     c) Print the names from 4th position to 9th position
     d) Print all names from 3rd position till end of the list
     e) Repeat list elements by specified number of times (N- times, where N is entered by user)
     f)  Concatenate two lists and print the output.
     g) Print element of list A and B side by side.(i.e. List-A First element, List-B First element )"""

listA = input("Enter the employee ID seperated by ',' \t").split(',')
listB = input("Enter the employee name seperated by ',' \t").split(',')

#a) Print all names on to screen
for name in listB:
    print(name)
    
#b) Read the index from the  user and print the corresponding name from both list.
print("Enter the index to print the list items:\n")
start = int(input("start"))
stop = int(input("stop"))
print(listA[start:stop])
print(listB[start:stop])

# c) Print the names from 4th position to 9th position
print(listB[4:10])

#d) Print all names from 3rd position till end of the list
print(listB[3:])

# e) Repeat list elements by specified number of times (N- times, where N is entered by user)
print("Enter n where n times the list will be repeated:\n")
n = int(input())
print(listA*n)
print(listB*n)

# f)  Concatenate two lists and print the output.
print(listA+listB)

#g) Print element of list A and B side by side.(i.e. List-A First element, List-B First element )"""
for id,name in zip(listA,listB):
    print(id,":",name)
