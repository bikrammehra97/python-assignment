"""
Create a list of 5 names and check given name exist in the List.
        a) Use membership operator (IN) to check the presence of an element.
        b) Perform above task without using membership operator.
        c) Print the elements of the list in reverse direction.

"""

names = input().split(',')
name = input("Enter a name to search in list:\t")
if name in names:
    print(name ,"is in List (names)")
for ele in names:
    if (name==ele):
        print("Name ",name," is in List (names)")
print(names[::-1])