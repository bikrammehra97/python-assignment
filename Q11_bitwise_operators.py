# Perform all bitwise operator on the numbers Q11
input1 = int(input())
input2 = int(input())

output = input1 & input2;
print(output)
output = input1 | input2;
print(output)
output = input1 ^ input2;
print(output)
output = ~input1;
print(output)
output = ~input2;
print(output)
output = input1 << 2;
print(output)
output = input2 << 2;
print(output)
output = input1 >> 2;
print(output)
output = input2 >> 2;
print(output)